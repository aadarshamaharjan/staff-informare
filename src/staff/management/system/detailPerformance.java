/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staff.management.system;

import java.awt.FlowLayout;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
/**
 *
 * @author Aadarsha
 */
public class detailPerformance extends javax.swing.JPanel {
    JPanel jp;
    int eid;
    DBConnection db;
    String name;
    Object data[][];
    /**
     * Creates new form detailPerformance
     */
    public detailPerformance() {
        initComponents();
    }
    public detailPerformance(JPanel jp, int eid, String name) {
        initComponents();
        this.jp=jp;
        this.eid=eid;
        this.name=name;
        db=new DBConnection();
        updateLabels();
        BarChart_AWT("Performance of "+name);
    }
    public void updateLabels(){
        data=db.selectPerformance(eid);
        jLabel17.setText(jLabel17.getText()+data[0][3]+" out of "+data[0][2]);
        jLabel16.setText(jLabel16.getText()+data[1][3]+" out of "+data[1][2]);
        jLabel15.setText(jLabel15.getText()+data[2][3]+" out of "+data[2][2]);
        jLabel14.setText(jLabel14.getText()+data[3][3]+" out of "+data[3][2]);
        jLabel13.setText(jLabel13.getText()+data[4][3]+" out of "+data[4][2]);
        jLabel12.setText(jLabel12.getText()+data[5][3]+" out of "+data[5][2]);
        jLabel11.setText(jLabel11.getText()+data[6][3]+" out of "+data[6][2]);
        jLabel10.setText(jLabel10.getText()+data[7][3]+" out of "+data[7][2]);
        jLabel9.setText(jLabel9.getText()+data[8][3]+" out of "+data[8][2]);
        jLabel8.setText(jLabel8.getText()+data[9][3]+" out of "+data[9][2]);
        jLabel7.setText(jLabel7.getText()+data[10][3]+" out of "+data[10][2]);
        jLabel6.setText(jLabel6.getText()+data[11][3]+" out of "+data[11][2]);
        jLabel5.setText(jLabel5.getText()+data[12][3]+" out of "+data[12][2]);
        jLabel4.setText(jLabel4.getText()+data[13][3]+" out of "+data[13][2]);
        jLabel3.setText(jLabel3.getText()+data[14][3]+" out of "+data[14][2]);
        jLabel2.setText(jLabel2.getText()+data[15][3]+" out of "+data[15][2]);
        jLabel1.setText(jLabel1.getText()+data[16][3]+" out of "+data[16][2]);
        
    }
    
    public void BarChart_AWT(String chartTitle ) { 
      JFreeChart barChart = ChartFactory.createBarChart(
         chartTitle,           
         "Category",            
         "Score",            
         createDataset(),          
         PlotOrientation.VERTICAL,           
         true, true, false);
         
      ChartPanel chartPanel = new ChartPanel( barChart );             
      chartPanel.setPreferredSize(new java.awt.Dimension(900,456) ); 
      jPanel1.removeAll();
      jPanel1.setLayout(new FlowLayout());
      jPanel1.add(chartPanel);
//      setContentPane( chartPanel ); 
   }
   
   private CategoryDataset createDataset() {
             
      DefaultCategoryDataset dataset = 
      new DefaultCategoryDataset( );  

      dataset.addValue( ((10.0/(int)data[16][2])*(int)data[16][3]) , "Rating" , "1. " );
      dataset.addValue( ((10.0/(int)data[15][2])*(int)data[15][3]) , "Rating" , "2. " );
      dataset.addValue( ((10.0/(int)data[14][2])*(int)data[14][3]) , "Rating" , "3. " );        
      dataset.addValue( ((10.0/(int)data[13][2])*(int)data[13][3]) , "Rating" , "4. " );        
      dataset.addValue( ((10.0/(int)data[12][2])*(int)data[12][3]) , "Rating" , "5. ");        
      dataset.addValue( ((10.0/(int)data[11][2])*(int)data[11][3]) , "Rating" , "6. " );        
      dataset.addValue( ((10.0/(int)data[10][2])*(int)data[10][3]) , "Rating" , "7. ");        
      dataset.addValue( ((10.0/(int)data[9][2])*(int)data[9][3]) , "Rating" , "8. " );        
      dataset.addValue( ((10.0/(int)data[8][2])*(int)data[8][3]) , "Rating" , "9. ");        
      dataset.addValue( ((10.0/(int)data[7][2])*(int)data[7][3]) , "Rating" , "10. " );        
      dataset.addValue( ((10.0/(int)data[6][2])*(int)data[6][3]) , "Rating" , "11. " );        
      dataset.addValue( ((10.0/(int)data[5][2])*(int)data[5][3]) , "Rating" , "12. ");        
      dataset.addValue( ((10.0/(int)data[4][2])*(int)data[4][3]) , "Rating" , "13. " );        
      dataset.addValue( ((10.0/(int)data[3][2])*(int)data[3][3]) , "Rating" , "14. " );        
      dataset.addValue( ((10.0/(int)data[2][2])*(int)data[2][3]) , "Rating" , "15. ");        
      dataset.addValue( ((10.0/(int)data[1][2])*(int)data[1][3]) , "Rating" , "16. ");        
      dataset.addValue( ((10.0/(int)data[0][2])*(int)data[0][3]) , "Rating" , "17. ");        
      return dataset; 
   }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();

        setBackground(new java.awt.Color(236, 236, 255));

        jPanel1.setBackground(new java.awt.Color(236, 236, 255));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 456, Short.MAX_VALUE)
        );

        jPanel2.setBackground(new java.awt.Color(236, 236, 255));
        jPanel2.setLayout(new java.awt.GridLayout(9, 2));

        jLabel1.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel1.setText("1. Regularity: ");
        jPanel2.add(jLabel1);

        jLabel2.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel2.setText("2. Punctuality: ");
        jPanel2.add(jLabel2);

        jLabel3.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel3.setText("3. Parent's Feedback: ");
        jPanel2.add(jLabel3);

        jLabel4.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel4.setText("4. Colleague's Feedback: ");
        jPanel2.add(jLabel4);

        jLabel5.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel5.setText("5. Students Understanding: ");
        jPanel2.add(jLabel5);

        jLabel6.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel6.setText("6. Classwork Evaluation: ");
        jPanel2.add(jLabel6);

        jLabel7.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel7.setText("7. Homework Evaluation: ");
        jPanel2.add(jLabel7);

        jLabel8.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel8.setText("8. Student English Speaking and Handwriting: ");
        jPanel2.add(jLabel8);

        jLabel9.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel9.setText(" 9. Student Discipline: ");
        jPanel2.add(jLabel9);

        jLabel10.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel10.setText("10. Student Cleanliness: ");
        jPanel2.add(jLabel10);

        jLabel11.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel11.setText("11. Diary Checking: ");
        jPanel2.add(jLabel11);

        jLabel12.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel12.setText("12. Daily Reocord: ");
        jPanel2.add(jLabel12);

        jLabel13.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel13.setText("13. Enchargeship Evaluation: ");
        jPanel2.add(jLabel13);

        jLabel14.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel14.setText("14. English Environment: ");
        jPanel2.add(jLabel14);

        jLabel15.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel15.setText("15. Uniform Grading: ");
        jPanel2.add(jLabel15);

        jLabel16.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel16.setText("16. Administation Poll: ");
        jPanel2.add(jLabel16);

        jLabel17.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jLabel17.setText("17. Management Poll: ");
        jPanel2.add(jLabel17);

        jPanel3.setBackground(new java.awt.Color(236, 236, 255));

        jButton1.setBackground(new java.awt.Color(236, 236, 255));
        jButton1.setFont(new java.awt.Font("Myanmar Text", 0, 18)); // NOI18N
        jButton1.setText("Back");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jButton1))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 834, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        jp.removeAll();
        jp.setLayout(new FlowLayout());
        jp.add(new viewPerformances(eid,name,jp));
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    // End of variables declaration//GEN-END:variables
}
