/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staff.management.system;

import javax.swing.JOptionPane;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Aadarsha
 */
public class Verification {
    boolean success;
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public Verification(){
        success=true;
    }
    public void dateValidation(String dateToValidate, String dateFromat){
        if(dateToValidate == null){
            success=false;
        }
	SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
	sdf.setLenient(false);
	try {
            //if not valid, it will throw ParseException
            Date current=new Date();
            Date date = sdf.parse(dateToValidate);
            if(date.after(current)){
                success=false;
            }
        } 
        catch (ParseException e) {
            e.printStackTrace();
            success=false;
	}
    }
    public void mobileValidation(String mobile){
        if (!mobile.matches("98\\d{10}")) success=false;
    }
    
    public void validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        if(!matcher.find()) success=false;
    }
    
    
}
