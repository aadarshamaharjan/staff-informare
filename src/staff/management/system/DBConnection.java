/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staff.management.system;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Aadarsha
 */
public class DBConnection {
    Connection con;
    Statement st;
    ResultSet rs;
    PreparedStatement insert_employee, insert_skill,insert_salary,insert_employee_dept,insert_employee_perform,insert_employee_subject;
    PreparedStatement select_employee, select_employee_department,select_employee_job_description,select_performance_latest,select_past_performance,select_past_all_performance;
    PreparedStatement get_employee, get_no_employee;
    PreparedStatement update_employee;
    String name;
    int eid;
    private PreparedStatement insert_employee_job_desc;
    private PreparedStatement insert_employee_performance;
     public void dbConnection(){
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("driver loaded");
        }catch(Exception e){
            System.out.println("driver not loaded");
        }
        try{
            con=DriverManager.getConnection("jdbc:mysql://localhost/staff_informare","root","");
            System.out.println("database loaded");
        }catch(Exception e){
            System.out.println("database not loaded");
        }
        try{
            st=con.createStatement();
            
        }catch(Exception e){
            System.out.println("error"+e);
        }
        try {
            insert_employee=con.prepareStatement("INSERT INTO employee(fname,mname,lname,city,state,ward,street,dob,gender,email,m_status,telephone,mobile,school,plus_two,bachelors,master) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            select_employee=con.prepareStatement("SELECT * FROM employee");
            get_employee=con.prepareStatement("SELECT * FROM employee AS e INNER JOIN salary AS s INNER JOIN skill AS sk INNER JOIN subject "
                    + "INNER JOIN job_description AS jd "
                    + "INNER JOIN employee_subject AS es "
                    + "INNER JOIN employee_job_description AS ejd " 
                    + "INNER JOIN employee_department AS ed " 
                    + "INNER JOIN department AS d "
                    + "WHERE id_job_description=job_description_id " 
                    + "AND s.employee_id=e.id_employee AND e.id_employee=sk.employee_id AND subject_id=id_subject AND id_department=department_id " 
                    + "AND ejd.employee_id=id_employee AND es.employee_id=id_employee AND ed.employee_id=id_employee AND id_employee=?");
            update_employee=con.prepareStatement("UPDATE employee SET fname=?,mname=?,lname=?,city=?,state=?,ward=?,street=?,dob=?,gender=?,m_status=?,email=?,telephone=?,mobile=?,school=?,plus_two=?,bachelors=?,master=? WHERE id_employee=?");
            insert_skill=con.prepareStatement("INSERT INTO skill(language, training, experience, employee_id) VALUES(?,?,?,?)");
            //insert_department=con.prepareStatement("INSERT INTO department(name) VALUES(?)");
            insert_employee_dept=con.prepareStatement("INSERT INTO employee_department(employee_id,department_id) VALUES(?,?)");
            insert_employee_job_desc=con.prepareStatement("INSERT INTO employee_job_description(employee_id,job_description_id) VALUES(?,?)");
            insert_employee_performance=con.prepareStatement("INSERT INTO employee_performance(employee_id,performance_id,obtained_marks) VALUES(?,?,?)");
            insert_employee_subject=con.prepareStatement("INSERT INTO employee_subject(subject_id,employee_id) VALUES(?,?)");
            //insert_job_description=con.prepareStatement("INSERT INTO employee_department() VALUES()");
            //insert_performance=con.prepareStatement("INSERT INTO employee_department() VALUES()");
            insert_salary=con.prepareStatement("INSERT INTO salary(bsalary,providentfund,insurance,food,tax,employee_id) VALUES(?,?,?,?,?,?)");
            
            select_performance_latest=con.prepareStatement("SELECT * FROM employee_performance AS e INNER JOIN performance AS p WHERE e.performance_id=p.id_performance AND employee_id=? ORDER BY created_at, performance_id DESC LIMIT 17");
            select_past_performance=con.prepareStatement("SELECT SUM(obtained_marks), MONTH(created_at), created_at FROM employee_performance WHERE YEAR(created_at)=YEAR(NOW()) AND employee_id=? GROUP BY MONTH(created_at) HAVING DATE(created_at)>=DATE(NOW())-INTERVAL 4 MONTH ORDER BY created_at DESC");
            select_past_all_performance=con.prepareStatement("SELECT SUM(obtained_marks), MONTH(created_at), created_at FROM employee_performance WHERE YEAR(created_at)=YEAR(NOW()) GROUP BY MONTH(created_at) HAVING DATE(created_at)>=DATE(NOW())-INTERVAL 4 MONTH ORDER BY created_at DESC");
            get_no_employee=con.prepareStatement("SELECT COUNT(*) FROM employee");
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
     Statement statementObject(){
         dbConnection();
         return st;
     }
     public void dbconreset(){
         try{
            st.close();
            con.close();
         }
         catch(Exception e){
             System.out.println("Error closing connection to DB");
         }
     }
     
    public Object[][] selectEmployees(){
        if(con==null){
            dbConnection();
        }
        try{
            ResultSet rs = select_employee.executeQuery();
            rs.last();
            int n = rs.getRow();
            rs.beforeFirst();
            Object [][] data = new Object[n][18];
            int i=0;
            while(rs.next()){
                data[i][0]=rs.getInt(1);
                data[i][1]=rs.getString(2);
                data[i][2]=rs.getString(3);
                data[i][3]=rs.getString(4); data[i][4]=rs.getString(5); data[i][5]=rs.getString(6); data[i][6]=rs.getString(7);
                data[i][7]=rs.getString(8);data[i][8]=rs.getString(9); data[i][9]=rs.getString(10);  data[i][10]=rs.getString(11);
                 data[i][11]=rs.getString(12);  data[i][12]=rs.getString(13);  data[i][13]=rs.getString(14);  data[i][14]=rs.getString(15);
                 data[i][15]=rs.getString(16);  data[i][16]=rs.getString(17);  data[i++][17]=rs.getString(18);
            }
            return data;
        }catch(Exception e){
            System.out.println("Error selecting employee");
            return null;
        }
        
    }
    
    
      public Object[][] getEmployee(int eid){
        if(con==null){
            dbConnection();
        }
        this.eid=eid;
        try{
            get_employee.setInt(1,eid);
            ResultSet rs = get_employee.executeQuery();
            rs.last();
            int n = rs.getRow();
            rs.beforeFirst();
            Object [][] data = new Object[n][28];
            int i=0;
            while(rs.next()){
                data[i][0]=rs.getString("fname");
                data[i][1]=rs.getString("mname");
                data[i][2]=rs.getString("lname");
                data[i][3]=rs.getString("dob");
                data[i][4] = rs.getString("city");
                data[i][5]=rs.getString("state");
                data[i][6]=rs.getString("ward");
                data[i][7]=rs.getString("street");
                data[i][8]=rs.getString("gender");
                data[i][9]=rs.getString("email");
                data[i][10]=rs.getString("m_status");
                data[i][11]=rs.getString("bsalary");
                data[i][12]=rs.getString("providentfund");
                data[i][13]=rs.getString("insurance");
                data[i][14]=rs.getString("gross_salary");
                data[i][15]=rs.getString("net_salary");
                data[i][16]=rs.getString("job_designation");
                data[i][17]=rs.getString("tax");
                data[i][18]=rs.getString("food");
                data[i][19]=rs.getString("school");
                data[i][20]=rs.getString("plus_two");
                data[i][21]=rs.getString("bachelors");
                data[i][22]=rs.getString("master");
                data[i][23]=rs.getString("language");
                data[i][24]=rs.getString("training");
                data[i][25]=rs.getString("experience");
                data[i][26]=rs.getString("telephone");
                data[i][27]=rs.getString("mobile");
            }
            return data;
        }catch(Exception e){
            System.out.println("Error selecting employee");
            return null;
        }
        
    }
      
    public int getNoEmployee(){
        if(con==null){
            dbConnection();
        }
        this.eid=eid;
        try{
            ResultSet rs = get_no_employee.executeQuery();
            rs.last();
            int n = rs.getRow(), emp = 0;
            rs.beforeFirst();
            while(rs.next()){
                emp=rs.getInt(1);
            }
            return emp;
        }catch(Exception e){
            System.out.println("Error selecting employee");
            return 0;
        }
    }
    public void insertEmployee(String fname, String mname, String lname, String city, String state, String ward, String street,String dob, String gender, String m_status, String email, String telephone, String mobile, String school, String plustwo, String bachelors, String masters){
        if(con==null){
            dbConnection();
        }
        try {
            insert_employee.setString(1,fname);
            insert_employee.setString(2,mname);
            insert_employee.setString(3,lname);
            insert_employee.setString(4,city);
            insert_employee.setString(5,state);
            insert_employee.setString(6,ward);
            insert_employee.setString(7,street);
            insert_employee.setString(8,dob);
            insert_employee.setString(9,gender);
            insert_employee.setString(10,email);
            insert_employee.setString(11,m_status);
            insert_employee.setString(12,telephone);
            insert_employee.setString(13,mobile);
            insert_employee.setString(14,school);
            insert_employee.setString(15,plustwo);
            insert_employee.setString(16,bachelors);
            insert_employee.setString(17,masters); 
            insert_employee.execute();
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void updateEmployee(String fname, String mname, String lname, String city, String state, String ward, String street,String dob, String gender, String m_status, String email, String telephone, String mobile, String school, String plustwo, String bachelors, String master, int eid){
        this.eid=eid;
        if(con==null){
            dbConnection();
        }
        try {
            update_employee.setString(1,fname);
            update_employee.setString(2,mname);
            update_employee.setString(3,lname);
            update_employee.setString(4,city);
            update_employee.setString(5,state);
            update_employee.setString(6,ward);
            update_employee.setString(7,street);
            update_employee.setString(8,dob);
            update_employee.setString(9,gender);
            update_employee.setString(10,email);
            update_employee.setString(11,m_status);
            update_employee.setString(12,telephone);
            update_employee.setString(13,mobile);
            update_employee.setString(14,school);
            update_employee.setString(15,plustwo);
            update_employee.setString(16,bachelors);
            update_employee.setString(17,master);    
            update_employee.setInt(18,eid);
            update_employee.executeUpdate();
            JOptionPane.showMessageDialog(null,"Successfully Updated!");
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    void insertEmployeeDept(int employee_id,int department_id){
        this.eid=eid;
        if(con==null){
            dbConnection();
        }
        try{
            insert_employee_dept.setInt(1, employee_id);
            insert_employee_dept.setInt(2, department_id);
            insert_employee_dept.execute();
        }catch(Exception e){
            
        }
    }
    
    void selectEmployeeDept(int eid){
        
    }
    void insertEmployeeJobDescription(int employee_id,int job_description_id){
        this.eid=eid;
        if(con==null){
            dbConnection();
        }
        try{
            insert_employee_job_desc.setInt(1,employee_id);
            insert_employee_job_desc.setInt(2,job_description_id);
            insert_employee_job_desc.execute();
        }catch(Exception e){
            
        }
    }
    
    
    boolean insertEmployeePerformance(int employee_id,int performance_id,int obtained_marks){
        //this.eid=eid;
        if(con==null){
            dbConnection();
        }
        try{
            insert_employee_performance.setInt(1,employee_id);
            insert_employee_performance.setInt(2,performance_id);
            insert_employee_performance.setInt(3,obtained_marks);
            insert_employee_performance.execute();
            return true;
        }catch(Exception e){
            return false;
        }
    }
    
    
    void insertEmployeeSubject(int subject_id,int employee_id){
        this.eid=eid;
        if(con==null){
            dbConnection();
        }
        try{
            insert_employee_subject.setInt(1,subject_id);
            insert_employee_subject.setInt(2,employee_id);
            insert_employee_subject.execute();
        }catch(Exception e){
            
        }
    }
    
    
    void insertSalary(String bsalary,String providentfund,String insurance,String food,String tax,int eid){
        this.eid=eid;
        if(con==null){
            dbConnection();
        }
        try{
            insert_salary.setString(1, bsalary);
            insert_salary.setString(2, providentfund);
            insert_salary.setString(3, insurance);
            insert_salary.setString(4, food);
            insert_salary.setString(5, tax);
            insert_salary.setInt(6, eid);
            insert_salary.execute();
        }catch(Exception e){
            
        }
    }
    
    
    void insertSkill(String language, String training, String experience, int eid){
        this.eid=eid;
        if(con==null){
            dbConnection();
        }
        try{
            insert_skill.setString(1, language);
            insert_skill.setString(2, training);
            insert_skill.setString(3, experience);
            insert_skill.setInt(4, eid);            
            insert_skill.execute();
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    void selectEmployeeInfo(){
        if(con==null){
            dbConnection();
        }
        try{
            get_employee.setInt(1,eid);
            ResultSet rs = get_employee.executeQuery();
            rs.last();
            int n = rs.getRow();
            rs.beforeFirst();
            Object [][] data = new Object[n][28];
            int i=0;
            while(rs.next()){
                data[i][0]=rs.getString("fname");
                data[i][1]=rs.getString("mname");
                data[i][2]=rs.getString("lname");
                data[i][3]=rs.getString("dob");
                data[i][4] = rs.getString("city");
                data[i][5]=rs.getString("state");
                data[i][6]=rs.getString("ward");
                data[i][7]=rs.getString("street");
                data[i][8]=rs.getString("gender");
                data[i][9]=rs.getString("email");
                data[i][10]=rs.getString("m_status");
                data[i][11]=rs.getString("bsalary");
                data[i][12]=rs.getString("providentfund");
                data[i][13]=rs.getString("insurance");
                data[i][14]=rs.getString("gross_salary");
                data[i][15]=rs.getString("net_salary");
                data[i][16]=rs.getString("job_designation");
                data[i][17]=rs.getString("tax");
                data[i][18]=rs.getString("food");
                data[i][19]=rs.getString("school");
                data[i][20]=rs.getString("plus_two");
                data[i][21]=rs.getString("bachelors");
                data[i][22]=rs.getString("master");
                data[i][23]=rs.getString("language");
                data[i][24]=rs.getString("training");
                data[i][25]=rs.getString("experience");
                data[i][26]=rs.getString("telephone");
                data[i][27]=rs.getString("mobile");
            }
           // return data;
        }catch(Exception e){
            System.out.println("Error selecting employee");
            //return null;
        }
    }
    
    public Object[][] selectPerformance(int eid){
        int sum=0;
        Object [][] data;
        if(con==null){
            dbConnection();
        }
        try{
            select_performance_latest.setInt(1,eid);
            ResultSet rs = select_performance_latest.executeQuery();
            rs.last();
            int n = rs.getRow();
            rs.beforeFirst();
            data = new Object[n][4];
            int i=0;
            while(rs.next()){
                data[i][0]=rs.getInt("performance_id");
                data[i][1]=rs.getString("title");
                data[i][2]=rs.getInt("full_marks");
                data[i][3]=rs.getInt("obtained_marks");
                sum+=(int)data[i][3];
                i++;
            }
            return data;
        }catch(Exception e){
            System.out.println("Error selecting employee "+e);
            return null;
        }
    }
    
    public Object[][] selectPastPerformance(int eid){
        Object [][] data;
        if(con==null){
            dbConnection();
        }
        try{
            select_past_performance.setInt(1,eid);
            ResultSet rs = select_past_performance.executeQuery();
            rs.last();
            int n = rs.getRow();
            rs.beforeFirst();
            data = new Object[n][2];
            int i=0;
            while(rs.next()){
                data[i][0]=rs.getInt(1);
                data[i][1]=rs.getInt(2);
                i++;
            }
            return data;
        }catch(Exception e){
            System.out.println("Error selecting employee in view performance"+e);
            return null;
        }
    }
    public Object[][] selectallPastPerformance(int eid){
        Object [][] data;
        if(con==null){
            dbConnection();
        }
        try{
//            select_past_all_performance.setInt(1,eid);
            ResultSet rs = select_past_all_performance.executeQuery();
            rs.last();
            int n = rs.getRow();
            rs.beforeFirst();
            data = new Object[n][2];
            int i=0;
            while(rs.next()){
                data[i][0]=rs.getInt(1);
                data[i][1]=rs.getInt(2);
                i++;
            }
            return data;
        }catch(Exception e){
            System.out.println("Error selecting employee (HomePanel)"+e);
            return null;
        }
    }
     /*milauna baki xa DB
     public static void connectDB(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("driver loaded");
        }catch(Exception e){
            System.out.println("driver not loaded");
        }
        try{
            con=DriverManager.getConnection("jdbc:mysql://localhost/staff informare","root","");
            System.out.println("database loaded");
        }catch(Exception e){
            System.out.println("database not loaded");
        }
    }
     public static Statement statementObject(){
        st=null;
        try{
               st=con.createStatement();

           }
        catch(Exception e){
            System.out.println("error"+e);
        }
        return st;
     }
     */

    public static void main(String[] args) {
        DBConnection db=new DBConnection();
        //db.selectEmployees();
        db.selectPerformance(7);
    }
}

