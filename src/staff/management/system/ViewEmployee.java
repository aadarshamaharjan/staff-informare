/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staff.management.system;

import java.awt.FlowLayout;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Aadarsha
 */
public class ViewEmployee extends javax.swing.JPanel {
    DBConnection db;
    Statement stmt;
    ResultSet rs;
    String panel;
    /**
     * Creates new form ViewEmployee
     */
    public ViewEmployee(JPanel jp, String panel) {
        initComponents();
        jPanel3.removeAll();
        jPanel3.setLayout(new FlowLayout());
        jPanel3.add(jp);
        jPanel3.revalidate();
        db=new DBConnection();
        this.panel=panel;
        changeTable("");  
        if(panel.equals("viewemp")){
        jTable1.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent event) {
                // do some actions here, for example
                // print first column value from selected row
                //new infoPanel(infoPane,jTable1.getValueAt(jTable1.getSelectedRow(),0).toString())
                jPanel3.removeAll();
                jPanel3.setLayout(new FlowLayout());
                int row=jTable1.getSelectedRow();
                if(row==-1){
                    jPanel3.add(new infoPanel(jPanel3));
                }
                else{
                    jPanel3.add(new infoPanel((int) jTable1.getValueAt(row,0)));
                }
                jPanel3.revalidate();
            }
        });    
        jTable1.getColumnModel().getColumn(0).setPreferredWidth(30);
        jTable1.getColumnModel().getColumn(1).setPreferredWidth(150);
        jTable1.getColumnModel().getColumn(2).setPreferredWidth(100);
        }
        else if(panel.equals("addper")){
            jTable1.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent event) {
                // do some actions here, for example
                // print first column value from selected row
                //new infoPanel(infoPane,jTable1.getValueAt(jTable1.getSelectedRow(),0).toString())
                jPanel3.removeAll();
                jPanel3.setLayout(new FlowLayout());
                int row=jTable1.getSelectedRow();
                if(row==-1){
                    jPanel3.add(new addPerformance());
                }
                else{
                    jPanel3.add(new addPerformance((int) jTable1.getValueAt(row,0),(String)jTable1.getValueAt(row,1)));
                }
                jPanel3.revalidate();
            }
        });    
        jTable1.getColumnModel().getColumn(0).setPreferredWidth(30);
        jTable1.getColumnModel().getColumn(1).setPreferredWidth(150);
        jTable1.getColumnModel().getColumn(2).setPreferredWidth(100);
        }
        else if(panel.equals("perview")){
            jTable1.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent event) {
                // do some actions here, for example
                // print first column value from selected row
                //new infoPanel(infoPane,jTable1.getValueAt(jTable1.getSelectedRow(),0).toString())
                jPanel3.removeAll();
                jPanel3.setLayout(new FlowLayout());
                int row=jTable1.getSelectedRow();
                if(row==-1){
                    jPanel3.add(new viewPerformances());
                }
                else{
                    jPanel3.add(new viewPerformances((int) jTable1.getValueAt(row,0),(String)jTable1.getValueAt(row,1), jPanel3));
                }
                jPanel3.revalidate();
            }
        });    
        jTable1.getColumnModel().getColumn(0).setPreferredWidth(30);
        jTable1.getColumnModel().getColumn(1).setPreferredWidth(150);
        jTable1.getColumnModel().getColumn(2).setPreferredWidth(100);
        }
    }
    
    public void changeTable(String name){
        Object[] ob={};
        DefaultTableModel tm;
        tm=(DefaultTableModel) jTable1.getModel();
        tm.setRowCount(0);
        stmt=db.statementObject();
        String sql="";
        if(panel.equals("viewemp")){
            sql="SELECT id_employee,fname,mname,lname,job_designation FROM employee AS e INNER JOIN job_description AS j INNER JOIN employee_job_description AS ej WHERE id_employee=employee_id AND id_job_description=job_description_id AND fname LIKE '"+name+"%' ORDER BY job_description_id ";
        }
        else{
            sql="SELECT id_employee,fname,mname,lname,job_designation FROM employee AS e INNER JOIN job_description AS j INNER JOIN employee_job_description AS ej WHERE id_employee=employee_id AND id_job_description=job_description_id AND job_designation='Teacher' ORDER BY job_description_id";
        }
        try{
            rs=stmt.executeQuery(sql);
            while(rs.next()){
                tm.addRow(new Object[]{rs.getInt("id_employee"),rs.getString("fname")+" "+rs.getString("mname")+" "+rs.getString("lname"),rs.getString("job_designation")});
            }
        }
        catch(SQLException e){
            System.out.println("Unable to populate table   "+e);
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jTextField1 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();

        setPreferredSize(new java.awt.Dimension(1440, 910));

        jPanel1.setBackground(new java.awt.Color(236, 236, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(1440, 910));

        jPanel2.setBackground(new java.awt.Color(236, 236, 255));

        jScrollPane1.setBackground(new java.awt.Color(236, 236, 255));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(280, 402));

        jTable1.setBackground(new java.awt.Color(236, 236, 255));
        jTable1.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "EID", "Name", "Designation"
            }
        ));
        jTable1.setRowHeight(70);
        jScrollPane1.setViewportView(jTable1);

        jTextField1.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                none(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField1KeyReleased(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Myanmar Text", 0, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Search.....");
        jLabel3.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        jLabel4.setFont(new java.awt.Font("Myanmar Text", 1, 36)); // NOI18N
        jLabel4.setText("Employee List");

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/staff-symbol.png"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(76, 76, 76)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel4))
                            .addComponent(jLabel5))
                        .addGap(0, 76, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 552, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel3.setBackground(new java.awt.Color(236, 236, 255));
        jPanel3.setPreferredSize(new java.awt.Dimension(859, 910));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 1013, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
//        jPanel3.removeAll();
//        jPanel3.add(new empInfo());
//        jPanel3.revalidate();
//        String name=jTextField1.getText().trim();
//        if(name.equals("")){
//            fetch();
//        }
//        else{
//            changeTable(name);
//        }
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void none(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_none
        // TODO add your handling code here:
       
    }//GEN-LAST:event_none

    private void jTextField1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyReleased
        // TODO add your handling code here:
         jPanel3.removeAll();
        jPanel3.add(new empInfo());
        jPanel3.revalidate();
        String name=jTextField1.getText().trim();
        if(name.equals("")){
            changeTable("");
        }
        else{
            changeTable(name);
        }
    }//GEN-LAST:event_jTextField1KeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
