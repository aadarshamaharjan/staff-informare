/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staff.management.system;

/**
 *
 * @author Aadarsha
 */
public class EmployeeModel extends AddEmployees{
    String fname, mname, lname;
    String city, state,ward, street;
    String year,month,date,dob;
    String gender;
    String mstatus;
    String email;
    String tele;
    String mob;
    String spercent,sinsti,school;        
    String plustwo, ppercent, pinsti;        
    String bachelors, bpercent, binsti;        
    String masters, mpercent, minsti;        
    String alanguage;                
    String atraining;        
    String aexperience;
    int position;
    int dept;
    int sub;
    String tax;
    String provident;
    String ainsurance;
    String food;
    double absalary;        
    int eid;
    String salarybt;
    String t_salary;
    DBConnection db; 
    
    public EmployeeModel(String fname, String mname, String lname, String acity, String astate, String award, String astreet, String year, String month, String date, String dob, String agender, String amstatus, String aemail, String tele, String mob, String spercent, String sinsti, String school, String plustwo, String ppercent, String pinsti, String bachelors, String bpercent, String binsti, String masters, String mpercent, String minsti, String alanguage, String atraining, String aexperience, int position, int dept, int sub, String tax, String provident, String ainsurance, String food, double absalary, int eid, String salarybt, String t_salary) {
        this.fname = fname;
        this.mname = mname;
        this.lname = lname;
        this.city = acity;
        this.state = astate;
        this.ward = award;
        this.street = astreet;
        this.year = year;
        this.month = month;
        this.date = date;
        this.dob = dob;
        this.gender = agender;
        this.mstatus = amstatus;
        this.email = aemail;
        this.tele = tele;
        this.mob = mob;
        this.spercent = spercent;
        this.sinsti = sinsti;
        this.school = school;
        this.plustwo = plustwo;
        this.ppercent = ppercent;
        this.pinsti = pinsti;
        this.bachelors = bachelors;
        this.bpercent = bpercent;
        this.binsti = binsti;
        this.masters = masters;
        this.mpercent = mpercent;
        this.minsti = minsti;
        this.alanguage = alanguage;
        this.atraining = atraining;
        this.aexperience = aexperience;
        this.position = position;
        this.dept = dept;
        this.sub = sub;
        this.tax = tax;
        this.provident = provident;
        this.ainsurance = ainsurance;
        this.food = food;
        this.absalary = absalary;
        this.eid = eid;
        this.salarybt = salarybt;
        this.t_salary = t_salary;
        db=new DBConnection();
    }
    EmployeeModel(){
        
    }
    void insertEmployee(){   
        db.insertEmployee(fname,mname, lname, city, state, ward, street,dob, gender, mstatus, email, tele, mob, school, plustwo, bachelors, masters);
    }
    void insertSkill(){
        db.insertSkill(alanguage,atraining,aexperience,eid);
    }
    void insertEmployeeDepartment(){
        //db.insert_employee_dept(eid,jdi);
    }
    
}
