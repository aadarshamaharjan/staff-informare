/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staff.management.system;

import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JPanel;

/**
 *
 * @author Aadarsha
 */
public class infoPanel extends javax.swing.JPanel {
    JPanel jp;
    int eid;
    Statement stmt;
    DBConnection db;
    String sql;
    String fname, mname, lname;
    private String city;
    private String dob;
    private String experience;
    private String training;
    private String language;
    private String master;
    private String bachelor;
    private String ptwo;
    private String school;
    private String food;
    private String tax;
    private String j_designation;
    private String t_salary;
    private String salarybt;
    private String insurance;
    private String pf;
    private String bsalary;
    private String amstatus;
    private String aemail;
    private String agender;
    private String street;
    private String ward;
    private String state;
    private String telephone, mobile;
    /**
     * Creates new form infoPanel
     */
    public infoPanel(JPanel jp) {
        initComponents();
        jp.removeAll();
        jp.add(new empInfo());
        jp.revalidate();
    }
    public infoPanel(int eid){
        initComponents();
        this.jp=jp;
        this.eid=eid;
        System.out.println(eid);
        db=new DBConnection();
        fname=""; mname=""; lname="";city="";dob="";experience="";training="";language="";master="";
        bachelor=""; ptwo="";school="";food="";tax="";j_designation="";t_salary="";salarybt="";insurance="";pf="";
        bsalary="";amstatus="";aemail="";agender="";street="";ward="";state="";telephone="";mobile="";
        getInfo();
        setLabels();
    }
    private void getInfo(){
        try{
            stmt=db.statementObject();
        }
        catch(Exception e){
            System.out.println("Error!! "+e);
        }
        try{
            sql="SELECT * FROM employee AS e INNER JOIN salary AS s INNER JOIN skill AS sk INNER JOIN subject "
                    + "INNER JOIN job_description AS jd "
                    + "INNER JOIN employee_subject AS es "
                    + "INNER JOIN employee_job_description AS ejd " 
                    + "INNER JOIN employee_department AS ed " 
                    + "INNER JOIN department AS d "
                    + "WHERE id_job_description=job_description_id " 
                    + "AND s.employee_id=e.id_employee AND e.id_employee=sk.employee_id AND subject_id=id_subject AND id_department=department_id " 
                    + "AND ejd.employee_id=id_employee AND es.employee_id=id_employee AND ed.employee_id=id_employee AND id_employee="+eid;
            ResultSet rs=stmt.executeQuery(sql);
            while(rs.next()){
                fname=rs.getString("fname");
                mname=rs.getString("mname");
                lname=rs.getString("lname");
                dob=rs.getString("dob");
                city = rs.getString("city");
                state=rs.getString("state");
                ward=rs.getString("ward");
                street=rs.getString("street");
                agender=rs.getString("gender");
                aemail=rs.getString("email");
                amstatus=rs.getString("m_status");
                bsalary=rs.getString("bsalary");
                pf=rs.getString("providentfund");
                insurance=rs.getString("insurance");
                salarybt=rs.getString("gross_salary");
                t_salary=rs.getString("net_salary");
                j_designation=rs.getString("job_designation");
                tax=rs.getString("tax");
                food=rs.getString("food");
                school=rs.getString("school");
                ptwo=rs.getString("plus_two");
                bachelor=rs.getString("bachelors");
                master=rs.getString("master");
                language=rs.getString("language");
                training=rs.getString("training");
                experience=rs.getString("experience");
                telephone=rs.getString("telephone");
                mobile=rs.getString("mobile");
            }
        }
        catch(Exception e){
            System.out.println("Unable to fetch data(infoPanel.java)  "+e);
        }
    }
    private void setLabels(){
        bname.setText(fname+" "+mname+" "+lname);
        name.setText(fname+" "+mname+" "+lname);
        address.setText("State "+state+", "+city+", "+street+", Ward No- "+ward);
        email.setText(aemail);
        gender.setText(agender);
        mstatus.setText(amstatus);
        tno.setText(telephone);
        mno.setText(mobile);
        designationl.setText(j_designation);
        totalsalary.setText(t_salary);
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        bname = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        name = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        gender = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        address = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        mstatus = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        email = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        tno = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        mno = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        designationl = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        totalsalary = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(236, 236, 255));

        jPanel1.setBackground(new java.awt.Color(236, 236, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(865, 910));

        jPanel3.setBackground(new java.awt.Color(236, 236, 255));

        jPanel7.setBackground(new java.awt.Color(236, 236, 255));
        jPanel7.setLayout(new java.awt.BorderLayout(5, 0));

        bname.setFont(new java.awt.Font("Myanmar Text", 1, 24)); // NOI18N
        bname.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        bname.setText("Person Name");
        jPanel7.add(bname, java.awt.BorderLayout.SOUTH);

        jLabel13.setFont(new java.awt.Font("Myanmar Text", 0, 48)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/user.png"))); // NOI18N
        jPanel7.add(jLabel13, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(169, 169, 169)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 512, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(184, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(236, 236, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Personal Information", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Myanmar Text", 0, 16))); // NOI18N
        jPanel2.setLayout(new java.awt.GridLayout(7, 2));

        jLabel1.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        jLabel1.setText("Name");
        jPanel2.add(jLabel1);

        name.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        name.setText("Name Here");
        jPanel2.add(name);

        jLabel3.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        jLabel3.setText("Gender");
        jPanel2.add(jLabel3);

        gender.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        gender.setText("Name Here");
        jPanel2.add(gender);

        jLabel9.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        jLabel9.setText("Address");
        jPanel2.add(jLabel9);

        address.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        address.setText("Name Here");
        jPanel2.add(address);

        jLabel4.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        jLabel4.setText("Marital Status");
        jPanel2.add(jLabel4);

        mstatus.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        mstatus.setText("Name Here");
        jPanel2.add(mstatus);

        jLabel8.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        jLabel8.setText("Email");
        jPanel2.add(jLabel8);

        email.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        email.setText("Name Here");
        jPanel2.add(email);

        jLabel6.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        jLabel6.setText("Telephone No.");
        jPanel2.add(jLabel6);

        tno.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        tno.setText("Name Here");
        jPanel2.add(tno);

        jLabel7.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        jLabel7.setText("Mobile No.");
        jPanel2.add(jLabel7);

        mno.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        mno.setText("Name Here");
        jPanel2.add(mno);

        jPanel4.setBackground(new java.awt.Color(236, 236, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Job Information", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Myanmar Text", 0, 14))); // NOI18N
        jPanel4.setLayout(new java.awt.GridLayout(2, 2));

        jLabel10.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        jLabel10.setText("Designation");
        jPanel4.add(jLabel10);

        designationl.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        designationl.setText("Name Here");
        jPanel4.add(designationl);

        jLabel11.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        jLabel11.setText("Total Salary");
        jPanel4.add(jLabel11);

        totalsalary.setFont(new java.awt.Font("Myanmar Text", 0, 16)); // NOI18N
        totalsalary.setText("Name Here");
        jPanel4.add(totalsalary);

        jPanel5.setBackground(new java.awt.Color(236, 236, 255));

        jPanel6.setBackground(new java.awt.Color(26, 35, 126));
        jPanel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel6MouseClicked(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Myanmar Text", 1, 16)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("View More......");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addComponent(jLabel12)
                .addContainerGap(76, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(52, Short.MAX_VALUE)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(710, 710, 710))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jPanel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MouseClicked
        // TODO add your handling code here:
        System.out.println("Clicked");
        
        new ViewEmployeeFrame(eid).setVisible(true);
    }//GEN-LAST:event_jPanel6MouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel address;
    private javax.swing.JLabel bname;
    private javax.swing.JLabel designationl;
    private javax.swing.JLabel email;
    private javax.swing.JLabel gender;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JLabel mno;
    private javax.swing.JLabel mstatus;
    private javax.swing.JLabel name;
    private javax.swing.JLabel tno;
    private javax.swing.JLabel totalsalary;
    // End of variables declaration//GEN-END:variables
}
